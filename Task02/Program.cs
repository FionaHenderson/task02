﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team07
{
    class MainClass
    {
        //public static int Counter { get; private set; }

        static void Main(string[] args)
        {
            GetStudentDetails();
        }

        static void GetStudentDetails()
        {
            Console.WriteLine("Hello, welcome to Bay of Plenty Polytechnic.\n \nThis Application will show you the Grades for the marks you have achieved for each paper, \nand also provide you with an average for all papers\n");
            int level;
            string studentID;
            int ID;

            while (true)
            {
                Console.Write("\nPlease begin by entering your 8 or 9 digit Student ID number: ");

                studentID = Console.ReadLine();

                bool IsValid = int.TryParse(studentID, out ID);
                int count = studentID.Length;
                if (count < 8 || count > 9 || IsValid == false)
                {
                    Console.WriteLine("/\nInvalid Student ID, Please Try Again!\n");
                    studentID = string.Empty;
                }
                else
                {
                    break;
                }
            }


            while (true)
            {
                Console.WriteLine("\nNow would you please tell me what level you are currently enrolled in.");
                Console.Write("\nEnter [5] for DAC5 or enter [6] for DAC6: ");
                string input = Console.ReadLine();

                bool IsValid = int.TryParse(input, out level);
                if (IsValid)
                {
                    if (level == 5)
                    {
                        UserInput(level, ID);
                        break;
                    }
                    else if (level == 6)
                    {
                        UserInput(level, ID);
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("\nThere was an error. Please ensure you enter [5] for DAC5 or [6] for DAC6.\n");
                    input = string.Empty;
                }
            }

            Console.ReadLine();
        }

        static void UserInput(int level, int studentID)
        {
            var dac5 = new string[8] { "\nCOMP5002: Introductory Programming", "COMP5007: Introduction to Multimedia", "COMP5008: Software Packages", "COMP4004: IT Infrastructure", "COMP5003: IT Essentials", "PROF5001: Professional Skills", "MATH5009: Statistics", "COMP5006: Electronics" };
            var dac6 = new string[8] { "\nCOMP6001: GUI Programming", "COMP6002: Internet and Web Dev", "COMP6008: Adv Programming", "BUSM6004: Applied Management", "BUSM6005: HR", "COMP2190: Databases", "COMP6019: Adv IT Infrastructure", "COMP6020: Adv Networking" };

            int Counter = 0;

            if (level == 5)
            {
                Counter = 4;
                Console.WriteLine($"\nPlease enter the 4 Digit Paper Code for each of the {Counter} DAC{level} papers that you are enrolled in.\nRefer to the list below for the ID numbers.");
                Console.WriteLine(string.Join("\n", dac5.Cast<string>()));
            }

            else if (level == 6)
            {
                Counter = 3;
                Console.WriteLine($"\nPlease enter the 4 Digit Paper Code for each of the {Counter} DAC{level} papers that you are enrolled in.\nRefer to the list below for the ID numbers.");
                Console.WriteLine(string.Join("\n", dac6.Cast<string>()));
            }

            var list = dac(Counter);

            ReturnUserInput(list, studentID, level, Counter);
        }

        static List<Tuple<string, double, string>> dac(int count)
        {
            var Dac = new List<Tuple<string, double, string>>();

            for (int i = 0; i < count; i++)
            {
                double Result;
                Console.Write($"\nPlease enter the 4 digit ID number for paper {i + 1}:");
                var ID = Console.ReadLine();
                var Grade = "";

                while (true)
                {
                    Console.Write($"\nPlease enter the result for paper {i + 1}: ");
                    var input = Console.ReadLine();

                    bool isNum = double.TryParse(input, out Result);

                    if (!isNum)
                    {
                        Console.WriteLine("\nInvalid Input. Please Try Again!");
                        input = string.Empty;
                    }
                    else
                    {
                        break;
                    }
                }
                if ((Result >= 90) && (Result <= 100))
                {
                    Grade = "A+";
                }
                else if ((Result >= 85) && (Result <= 89))
                {
                    Grade = "A";
                }
                else if ((Result >= 80) && (Result <= 84))
                {
                    Grade = "A-";
                }
                else if ((Result >= 75) && (Result <= 79))
                {
                    Grade = "B+";
                }
                else if ((Result >= 70) && (Result <= 74))
                {
                    Grade = "B";
                }
                else if ((Result >= 65) && (Result <= 69))
                {
                    Grade = "B-";
                }
                else if ((Result >= 60) && (Result <= 64))
                {
                    Grade = "C+";
                }
                else if ((Result >= 55) && (Result <= 59))
                {
                    Grade = "C";
                }
                else if ((Result >= 50) && (Result <= 54))
                {
                    Grade = "C-";
                }
                else if ((Result >= 40) && (Result <= 49))
                {
                    Grade = "D";
                }
                else if ((Result >= 0) && (Result <= 39))
                {
                    Grade = "E";
                }
                else
                {
                    break;
                }

                Dac.Add(Tuple.Create(ID, Result, Grade));
            }
            return Dac;
        }

        static void ReturnUserInput(List<Tuple<string, double, string>> list, int ID, int level, int Counter)
        {
            Console.WriteLine($"\nThe Paper ID's, Results for Each Paper and Corresponding Grades Are:\n{(string.Join(" ", list))}");
            Console.WriteLine($"\nYour Student ID is: {ID}");
            Console.WriteLine($"\nYou are enrolled in DAC{level}.");

            GetAverage(list, Counter, level);
        }

        static void GetAverage(List<Tuple<string, double, string>> list, int Counter, int level)
        {
            double Average = 0;

            foreach (var value in list)
            {
                Average += value.Item2;
            }
            Average /= Counter;
            Console.WriteLine($"\nThe Average Result for all your DAC{level} Papers is: {Math.Round(Average, 2)}%.");

            ReturnAPlusPapers(list, Counter);
        }

        static void ReturnAPlusPapers(List<Tuple<string, double, string>> list, int Counter)
        {
            var NoAPlus = new List<double>();

            foreach (var value in list)

                if ((value.Item2 >= 90) && (value.Item2 <= 100))
                {
                    Console.WriteLine($"\nCongratulations you have achieved an {value.Item3} for Paper:{value.Item1}, Result:{value.Item2}%");
                }
                else if (value.Item2 < 90)
                {
                    NoAPlus.Add(value.Item2);
                }
                else
                {
                    break;
                }

            if (Counter - NoAPlus.Count == 0)
            {
                Console.WriteLine("\nInfortunatly you did not achieve an A+ for any of your papers this year.\nTo achieve an A+, the combined result for each paper must be 90% or higher.\nYou'll have to try harder next year!");
            }
        }
    }
}
